@extends('layouts.master')

@section('title')
    <title>Management Role</title>
@endsection

@section('content')
    <div class="content-wrapper">
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="mb-0 text-dark">Management Role</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item">
                                <a href="{{rouye('home')}}">Home</a>
                            </li>
                            <li class="breadcrumb-item">Role</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-4">
                        @card
                            @slot('title')
                                Tambah
                            @endslot
                            @if (session('error'))
                                @alert(['type' => 'danger'])
                                    {!! session('error') !!}
                                @endalert
                            @endif

                            <form role="form" action="{{route('role.store')}}" method="post">
                                @csrf
                                <div class="form-group">
                                  <label for="">Role</label>
                                  <input type="text" name="name" id="" class="form-control {{$errors->has('name') ? 'is_invalid' : ''}}" placeholder="Role" required>
                                </div>
                            </form>
                            @slot('footer')
                                <div class="card-footer">
                                    <button class="btn btn-primary">Simpan</button>
                                </div>
                            @endslot
                        @endcard
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
